import knightImage from "../assets/cartKnight.png";
import plentyImage from "../assets/cartFind.png";
import roadsImage from "../assets/cartRoads.png";
import monopolyImage from "../assets/cartMonopoly.png";
import victoryImage from "../assets/cartPoint.png";

const resources = ['wood', 'wood', 'wood', 'wood', 'clay', 'clay', 'clay', 'sheep', 'sheep', 'sheep', 'sheep', 'wheat', 'wheat', 'wheat', 'wheat', 'stone', 'stone', 'stone', 'desert'];
const numbers = [2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12];

const tiles = resources.map((resource, i) => ({
  resource: resource,
  number: resource === 'desert' ? null : numbers[i]
}));

export const SHUFFLED_TILES = tiles.sort(() => Math.random() - 0.5);
export const SHUFFLED_NUMBERS = numbers.sort(() => Math.random() - 0.5);

export const RESOURCE_IMAGES = {
  wood: "src/assets/woodR.png",
  clay: "src/assets/clayR.png",
  sheep: "src/assets/sheepR.png",
  wheat: "src/assets/wheatR.png",
  stone: "src/assets/stockR.png",
  desert: "src/assets/desert.jpg",
  water: "src/assets/water.jpg",
};

export const harborResources = [
  { resource: 'wood', imagePath: 'src/assets/tree.jpg' },
  { resource: 'clay', imagePath: 'src/assets/clay.jpg' },
  { resource: 'sheep', imagePath: 'src/assets/sheepCard.jpg' },
  { resource: 'wheat', imagePath: 'src/assets/wheat.jpg' },
  { resource: 'stone', imagePath: 'src/assets/stone1.jpg' },
  { resource: '3:1', imagePath: null },
  { resource: '3:1', imagePath: null },
  { resource: '3:1', imagePath: null },
  { resource: '3:1', imagePath: null },
];
export const harborLocations = [0, 2, 5, 6, 9, 10, 13, 14, 16];

export const DEVELOPMENT_CARDS = [
  { type: "knight", quantity: 14, image: knightImage },
  { type: "plenty", quantity: 2, image: plentyImage },
  { type: "roads", quantity: 2, image: roadsImage },
  { type: "monopoly", quantity: 2, image: monopolyImage },
  { type: "victory", quantity: 5, image: victoryImage },
];

export const CARDS = DEVELOPMENT_CARDS.flatMap(card =>
  Array(card.quantity).fill(card.type)
);