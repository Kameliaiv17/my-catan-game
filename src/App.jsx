import { useState, useEffect } from "react";
import Card from "./components/Card";
import Tile from "./components/Tile";
import Player from "./components/Player";
import "./App.css";
import { CARDS, SHUFFLED_TILES, harborLocations, harborResources } from "./variables/gameConst";
import DiceComponent from "./components/DiceComponent";
import styled from "styled-components";
import DevelopmentCard from "./components/DevelopmentCard";

const shuffledHarborResources = harborResources.sort(() => Math.random() - 0.5);

const waterTiles = Array(18).fill({ resource: 'water', number: null, harbor: null }).map((tile, i) => {
  if (harborLocations.includes(i)) {
    const harborResource = shuffledHarborResources.shift();
    return { ...tile, harbor: harborResource };
  }
  return tile;
});

function App() {
  const [tiles, setTiles] = useState(SHUFFLED_TILES);
  const [drawnCards, setDrawnCards] = useState([]);
  const [cards, setCards] = useState(CARDS);

  useEffect(() => {
    let shuffledTiles = SHUFFLED_TILES.slice();
    let redNumbers = shuffledTiles.filter(tile => tile.number === 6 || tile.number === 8);
    let otherNumbers = shuffledTiles.filter(tile => tile.number !== 6 && tile.number !== 8);

    redNumbers.sort(() => Math.random() - 0.5);
    otherNumbers.sort(() => Math.random() - 0.5);

    let newTiles = [];
    let lastWasRed = false;
    while (redNumbers.length > 0 || otherNumbers.length > 0) {
      if (!lastWasRed && redNumbers.length > 0 && (otherNumbers.length === 0 || Math.random() < 0.5)) {
        newTiles.push(redNumbers.pop());
        lastWasRed = true;
      } else if (otherNumbers.length > 0) {
        newTiles.push(otherNumbers.pop());
        lastWasRed = false;
      }
    }

    setTiles(newTiles);
  }, []);

  const drawCard = () => {
    if (cards.length === 0) return;

    const randomIndex = Math.floor(Math.random() * cards.length);
    const drawnCard = cards[randomIndex];

    setCards(prevCards => prevCards.filter((_, i) => i !== randomIndex));
    setDrawnCards(prevCards => [...prevCards, drawnCard]);
  };


  const boardTiles = [
    ...waterTiles.slice(0, 4), // 1st row
    waterTiles[4], ...tiles.slice(0, 3), waterTiles[5], // 2nd row
    waterTiles[6], ...tiles.slice(3, 7), waterTiles[7], // 3rd row
    waterTiles[8], ...tiles.slice(7, 12), waterTiles[9], // 4th row
    waterTiles[10], ...tiles.slice(12, 16), waterTiles[11], // 5th row
    waterTiles[12], ...tiles.slice(16, 19), waterTiles[13], // 6th row
    ...waterTiles.slice(14, 18) // 7th row
  ];

  const rows = [
    boardTiles.slice(0, 4),
    boardTiles.slice(4, 9),
    boardTiles.slice(9, 15),
    boardTiles.slice(15, 22),
    boardTiles.slice(22, 28),
    boardTiles.slice(28, 33),
    boardTiles.slice(33, 37)
  ];

  return (
    <div className="App">
      <BoardContainer>
        <PlayerSide>
          <PlayerContainer>
            <Player name='Player 1' initialPieces={{ settlements: 5, cities: 4, roads: 15 }} color="blue" />
          </PlayerContainer>
        </PlayerSide>

        <BoardMiddle>
          <PlayerSide>
            <PlayerContainer>
              <Player name='Player 2' initialPieces={{ settlements: 5, cities: 4, roads: 15 }} color="green" flexDirection="row" rotation="-90" />
            </PlayerContainer>
          </PlayerSide>

          <Board>
            <CardAndDiceContainer>
              <button onClick={drawCard} className="draw-card-button">
                Draw a card
              </button>
              <div className="dice-container">
                <DiceComponent />
              </div>
            </CardAndDiceContainer>

            {rows.map((row, i) => (
              <Row key={i}>
                {row.map((tile, i) => (
                  <Tile
                    key={i}
                    resource={tile.resource}
                    number={tile.number}
                    harbor={tile.harbor}
                  />
                ))}
              </Row>
            ))}

            <HandContainer>
              {drawnCards.map((card, i) => (
                <DevelopmentCard
                  key={i}
                  type={card}
                />
              ))}
            </HandContainer>
          </Board>
          <PlayerSide>
            <PlayerContainer>
              <Player name='Player 3' initialPieces={{ settlements: 5, cities: 4, roads: 15 }} color="yellow" flexDirection="row" rotation="90" />
            </PlayerContainer>
          </PlayerSide>
        </BoardMiddle>
        <PlayerSide>
          <PlayerContainer>
            <Player name='Player 4' initialPieces={{ settlements: 5, cities: 4, roads: 15 }} color="orange" />
          </PlayerContainer>
        </PlayerSide>
      </BoardContainer>
    </div>
  );
}

export default App;

const BoardContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

const BoardMiddle = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
`;

const PlayerSide = styled.div`
  display: flex;
  padding: 30px;
  flex-direction: column;
  justify-content: center;
`;

const PlayerContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const Board = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Row = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: -30px; 
  &:nth-child(odd) {
    padding-left: 0; 
  }
`;

const CardAndDiceContainer = styled.div`
  display: flex;
  align-items: center;
  width: 171%;
  margin-bottom: 39px;
  flex-direction: row-reverse;
`;

const HandContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;