import styled from 'styled-components';

const StyledSettlement = styled.div`
  width: 20px;
  height: 20px;
  background-color: ${props => props.color || 'blue'};
`;

function Settlement({ color }) {
  return <StyledSettlement color={color} />;
}

export default Settlement;