import styled from 'styled-components';
import { RESOURCE_IMAGES } from '../variables/gameConst';
import Harbor from './Harbor';

const StyledTile = styled.div`
  position: relative;  
  width: 60px;
  height: 72.38px;
  background-size: cover;
  background-image: url(${props => RESOURCE_IMAGES[props.resource]});
  clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
`;

const NumberCircle = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background-color: #fff;
  border: 2px solid rgba(0, 0, 0, 0.75);
  color: ${props => (props.number === 6 || props.number === 8) ? 'red' : 'black'};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  font-weight: bold;
  margin: 5px;
`;

function Tile({ resource, number, harbor, harborPosition }) {
  return (
    <StyledTile resource={resource}>
      {number && <NumberCircle number={number}>{number}</NumberCircle>}
      {harbor && <Harbor resource={harbor} position={harborPosition} />}
    </StyledTile>
  );
}

export default Tile;