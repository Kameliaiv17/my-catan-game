function Card({ type }) {
  return (
    <div className="card">
      {type}
    </div>
  );
}

export default Card;
