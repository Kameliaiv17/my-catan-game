import styled from "styled-components";

const StyledPiece = styled.div`
  width: 30px;
  height: 30px;

  &.settlement {
    background-color: blue;
  }

  &.city {
    background-color: green;
  }

  &.road {
    background-color: red;
    width: 50px;
  }
`; 

function Piece({ type }) {
  
  return <StyledPiece className={`piece piece-${type}`} />;
}

export default Piece;
