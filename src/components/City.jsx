import styled from 'styled-components';

const StyledCity = styled.div`
  width: 25px;
  height: 25px;
  border-radius: 30%;
  background-color: ${props => props.color || 'green'};
`;

function City({ color }) {
  return <StyledCity color={color} />;
}

export default City;