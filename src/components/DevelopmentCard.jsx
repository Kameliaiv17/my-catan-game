import { DEVELOPMENT_CARDS } from "../variables/gameConst";
import styled from 'styled-components';

const CardContainer = styled.div`
  width: 200px;
  height: 300px;
  border-radius: 10px;
  overflow: hidden;
  box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.15);
  margin: 15px;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    width: 100%;
    height: auto;
  }
`;

function DevelopmentCard({ type }) {
  const cardInfo = DEVELOPMENT_CARDS.find(card => card.type === type);

  return (
    <CardContainer>
      <img src={cardInfo.image} alt={cardInfo.type} />
    </CardContainer>
  );
}

export default DevelopmentCard;

