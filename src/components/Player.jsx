import { useState } from 'react';
import Settlement from './Settlement';
import City from './City';
import Road from './Road';
import styled from 'styled-components';

const PlayerContainer = styled.div`
display: flex;
flex-wrap: nowrap;
flex-direction: column;
align-content: stretch;
justify-content: flex-start;
align-items: center;
width: 269px;
height: 124px;
  transform: rotate(${props => props.rotation || '0'}deg);
`;

const PlayerName = styled.h2`
  color: #000;
  margin-bottom: 20px;
`;

const PiecesContainer = styled.div`
  display: flex;
  flex-direction: ${props => props.piecesDirection || 'column'};
  align-items: center;
  gap: 10px;
`;

const PiecesRow = styled.div`
  display: flex;
  flex-direction: ${props => props.rowDirection || 'row'};
  align-items: center;
  gap: 10px;
`;

function Player({ name, initialPieces, piecesDirection, rowDirection, rotation, color }) {
  const [pieces, setPieces] = useState(initialPieces);

  const renderPieces = (Component, count, color) =>
    Array.from({ length: count }, (_, i) => <Component key={i} color={color} />);


  return (
    <PlayerContainer rotation={rotation}>
      <PlayerName>{name}</PlayerName>
      <PiecesContainer piecesDirection={piecesDirection}>
        <PiecesRow rowDirection={rowDirection}>
          {renderPieces(Settlement, pieces.settlements, color)}
        </PiecesRow>
        <PiecesRow rowDirection={rowDirection}>
          {renderPieces(City, pieces.cities, color)}
        </PiecesRow>
        <PiecesRow rowDirection={rowDirection}>
          {renderPieces(Road, pieces.roads, color)}
        </PiecesRow>
      </PiecesContainer>
    </PlayerContainer>
  );
}

export default Player;
