import styled from 'styled-components';

const StyledRoad = styled.div`
  width: 15px;
  height: 5px;
  background-color: ${props => props.color || 'red'};
`;

function Road({ color }) {
  return <StyledRoad color={color} />;
}

export default Road;