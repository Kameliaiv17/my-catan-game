import { useState } from 'react';
import dice1 from '../assets/image0.png';
import dice2 from '../assets/image1.png';
import dice3 from '../assets/image2.png';
import dice4 from '../assets/image3.png';
import dice5 from '../assets/image4.png';
import dice6 from '../assets/image5.png';
import { styled } from 'styled-components';


const DiceImage = styled.img`
  width: 50px;
  height: 50px;
`;

function DiceComponent() {
  const [diceNumber1, setDiceNumber1] = useState(1);
  const [diceNumber2, setDiceNumber2] = useState(1);

  const rollDice = () => {
    setDiceNumber1(Math.floor(Math.random() * 6) + 1);
    setDiceNumber2(Math.floor(Math.random() * 6) + 1);
  };

  const diceImages = [dice1, dice2, dice3, dice4, dice5, dice6];

  return (
    <div>
      <DiceImage src={diceImages[diceNumber1 - 1]} alt={`Dice ${diceNumber1}`} />
      <DiceImage src={diceImages[diceNumber2 - 1]} alt={`Dice ${diceNumber2}`} />
      <button onClick={rollDice}>Roll Dice</button>
    </div>
  );
}

export default DiceComponent;