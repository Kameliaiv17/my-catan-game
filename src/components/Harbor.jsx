import styled from 'styled-components';

const HarborCircle = styled.div`
position: absolute;
${props => {
    switch (props.position) {
      case 'left': return 'top: 23px;left: 15px;';
      case 'right': return 'top: 10px; right: 8px;';
      case 'top': return 'top: 23px;left: 15px;';
      case 'bottom': return 'bottom: 15px; right: 15px;';
      default: return 'top: 20px;left: 20px;';
    }
  }}
  width: 15px;
  height: 15px;
  border-radius: 50%;
  border: 4px solid rgba(0, 0, 0, 0.25);
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  margin: 5px;
  font-weight: bold;
  background-image: url(${props => props.backgroundImage});
  background-color: ${props => !props.backgroundImage ? '#c7d416' : 'transparent'};
`;

function Harbor({ resource, position }) {
  return (
    <HarborCircle position={position} backgroundImage={resource.imagePath}>
      {!resource.imagePath && <p>{resource.resource}</p>}
    </HarborCircle>
  );
}

export default Harbor;